/**
 * Created by nikita on 20.10.16.
 */

function getAllPosts() {
    var classXhr = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xhr = new classXhr();

    xhr.open('GET', 'http://localhost:3333/entry', false);
    xhr.send();

    if (xhr.status != 200) {
        console.log(xhr.status + " : " + xhr.statusText);
    } else {
        var obj = JSON.parse(xhr.response);
        var html = tplInHtml(obj, tpl);
        // alert(obj);
        console.log(html);
        console.log(obj);

        document.getElementById('dopHtml').innerHTML = html;
    }

}

function addPost(obj, callback) {
    var classXhr = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xhr = new classXhr();

    xhr.open('POST','http://localhost:3333/entry', false);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

    xhr.addEventListener('load', function () {
        callback(xhr.response);
    });

    xhr.send(JSON.stringify(obj));

}

function getPostById(ident) {
    var classXhr = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xhr = new classXhr();

    xhr.open('GET', 'http://localhost:3333/entry/' + ident, false);
    xhr.send();

    if (xhr.status != 200 ) {
        console.log(xhr.status + " : " + xhr.statusText);
    } else {
        var obj = JSON.parse(xhr.response);
        var list = [];
        list.push(obj);
        var html = tplInHtml(list, tpl);

        console.log('This is html file' + html);
        console.log(obj);

        document.getElementById('postViews').innerHTML = html;

    }
}

function updatePost(obj, callback) {
    var classXhr = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xhr = new classXhr();

    xhr.open('PUT','http://localhost:3333/entry', false);

    // if (xhr.status != 200 || xhr.status != 0) {
    //     console.log(xhr.status + " : " + xhr.statusText);
    // } else {
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xhr.addEventListener('load', function () {
            callback(xhr.response);
        });
        xhr.send(JSON.stringify(obj));
    // }
}


function deletePost(obj, callback) {
    var classXhr = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;
    var xhr = new classXhr();

    xhr.open('DELETE','http://localhost:3333/entry', false);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.addEventListener('load', function () {
        callback(xhr.response);
    });

    xhr.send(JSON.stringify(obj));
}