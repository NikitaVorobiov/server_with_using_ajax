/**
 * Created by hanni on 10/20/16.
 */

function getPostByIdBt() {
    document.getElementById('dopHtml').innerHTML = getPostHtml;
}

function updatePostBt() {
    document.getElementById('dopHtml').innerHTML = updateForm;
}

function deletePostBt() {
    document.getElementById('dopHtml').innerHTML = deletePostHtml;
}

function addNewPostBt() {
    document.getElementById('dopHtml').innerHTML = addPostHtml;
}

function addingData() {
    var obj = {};

    obj.Author = document.getElementById('author').value;
    obj.Title = document.getElementById('title').value;
    obj.Text = document.getElementById('text').value;

    addPost(obj, function (element) {
        console.log(element);
    });

    alert('Added new post!');

    document.getElementById('dopHtml').innerHTML = ``;
}

function updatedData() {
    var obj = {};

    obj.EntryId = document.getElementById('idPost').value;
    obj.Author = document.getElementById('author').value;
    obj.Text = document.getElementById('text').value;
    obj.Title = document.getElementById('title').value;

    updatePost(obj, function (element) {
        console.log(element);
    });

    alert('Data updated!');

    document.getElementById('dopHtml').innerHTML = `<p id="postViews"></p>`;
    getPostById(obj.EntryId);
}

function deletedData() {
    var obj = {};

    obj.EntryId = document.getElementById('idForfunc').value;

    deletePost(obj, function (element) {
        console.log(element);
    });

    document.getElementById('dopHtml').innerHTML = ``;
}

function tplInHtml(list, tpl) {
    var rezult = ``;

    for (var i = 0; i < list.length; i++) {
        var begin = tpl;

        while (begin.indexOf(`{{`) != -1) {
            rezult += begin.slice(0, begin.indexOf(`{{`));
            rezult += list[i][ begin.slice(begin.indexOf(`{{`) + 2, begin.indexOf(`}}`)) ];
            begin = begin.slice( begin.indexOf(`}}`) + 2, begin.length );

            if (begin.indexOf(`{{`) == -1) {
                rezult += begin;
            }
        }
    }
    return rezult;
}


